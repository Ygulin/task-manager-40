package ru.tsc.gulin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.gulin.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.gulin.tm.api.endpoint.IUserEndpoint;
import ru.tsc.gulin.tm.dto.request.*;
import ru.tsc.gulin.tm.dto.response.*;
import ru.tsc.gulin.tm.marker.SoapCategory;
import ru.tsc.gulin.tm.model.User;

@Category(SoapCategory.class)
public class UserEndpointTest {

    /* @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @Nullable
    private String token;

    @Nullable
    private String adminToken;


    @Before
    public void init() {
        token = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
        adminToken = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
    }

    @Test
    public void changeUserPassword() {
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.changeUserPassword(new UserChangePasswordRequest("", "pass"))
        );
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.changeUserPassword(new UserChangePasswordRequest(token, ""))
        );
        @NotNull final UserChangePasswordResponse response =
                userEndpoint.changeUserPassword(new UserChangePasswordRequest(token, "pass"));
        Assert.assertNotNull(response);
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "pass"));
        Assert.assertEquals(true, loginResponse.getSuccess());
        userEndpoint.changeUserPassword(new UserChangePasswordRequest(token, "test"));
    }

    @Test
    public void lockUser() {
        @NotNull final User user = userEndpoint.registryUser(new UserRegistryRequest(adminToken, "login lock", "pass", "email new")).getUser();
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(adminToken, "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(token, "")));
        userEndpoint.lockUser(new UserLockRequest(adminToken, "login lock"));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("login lock", "pass")));
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "login lock"));
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "", "pass", "new_email"))
        );
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "new_user", "", "new_email"))
        );
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "new_user", "pass", ""))
        );
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "test", "pass", "new_email"))
        );
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "new_user", "pass", "email"))
        );
        @NotNull final UserRegistryResponse response =
                userEndpoint.registryUser(new UserRegistryRequest(adminToken, "new_user", "pass", "new_email"));
        Assert.assertNotNull(response);
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "new_user"));
    }

    @Test
    public void removeUser() {
        userEndpoint.registryUser(new UserRegistryRequest(adminToken, "user remove", "pass", "email remove"));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(adminToken, "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(token, "")));
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "user remove"));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("user remove", "pass")));
    }

    @Test
    public void unlockUser() {
        @NotNull final User user = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "login unlock", "pass", "email unlock")
        ).getUser();
        userEndpoint.lockUser(new UserLockRequest(adminToken, "login unlock"));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(adminToken, "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(token, "")));
        userEndpoint.unlockUser(new UserUnlockRequest(adminToken, "login unlock"));
        Assert.assertEquals(true, authEndpoint.login(new UserLoginRequest("login unlock", "pass")).getSuccess());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "login unlock"));
    }

    @Test
    public void updateUserProfile() {
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.updateUserProfile(
                        new UserUpdateProfileRequest("", "Last", "First", "Middle")
                )
        );
        @NotNull final UserUpdateProfileResponse response =
                userEndpoint.updateUserProfile(new UserUpdateProfileRequest(token, "Last", "First", "Middle"));
        Assert.assertNotNull(response);
        Assert.assertEquals("First", response.getUser().getFirstName());
    } */

}
