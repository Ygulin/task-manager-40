package ru.tsc.gulin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Insert("INSERT INTO TM_USER (id, login, password, email, role, last_name, first_name, middle_name, lock)"
            + " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{role}, #{lastName}, #{firstName}, #{middleName}, #{locked})"
    )
    void add(@NotNull final User user);

    @Insert("INSERT INTO TM_USER (id, login, password, email, role, last_name, first_name, middle_name, lock)"
            + " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{role}, #{lastName}, #{firstName}, #{middleName}, #{locked})"
    )
    void addAll(@NotNull final Collection<User> users);

    @Update("UPDATE TM_USER SET login = #{login}, password = #{passwordHash}, email = #{email},"
            + " role = #{role}, last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middle_name}, "
            + "lock = #{locked} WHERE id = #{id}"
    )
    void update(@NotNull final User user);

    @NotNull
    @Select("SELECT id, login, password, email, role, last_name, first_name, middle_name, lock FROM TM_USER")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "lock")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT id, login, password, email, role, last_name, first_name, middle_name, lock FROM TM_USER"
            + " WHERE id = #{id}"
    )
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "lock")
    })
    User findOneById(@Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_USER WHERE id = #{id}")
    void remove(@NotNull final User user);

    @Delete("DELETE FROM TM_USER WHERE id = #{id}")
    void removeById(@Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_USER")
    void clear();

    @Select("SELECT count(1) = 1 FROM TM_USER WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull final String id);

    @Select("SELECT count(1) FROM TM_USER")
    long getSize();

    @Nullable
    @Select("SELECT id, login, password, email, role, last_name, first_name, middle_name, lock FROM TM_USER"
            + " WHERE login = #{login}"
    )
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "lock")
    })
    User findOneByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT id, login, password, email, role, last_name, first_name, middle_name, lock FROM TM_USER"
            + " WHERE email = #{email}"
    )
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "lock")
    })
    User findOneByEmail(@Param("email") @NotNull String email);

    @Delete("DELETE FROM TM_USER WHERE login = #{login}")
    void removeByLogin(@Param("login") @NotNull String login);

}
