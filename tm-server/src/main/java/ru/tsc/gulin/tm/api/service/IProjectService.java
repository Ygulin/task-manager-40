package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateStart,
            @Nullable Date dateEnd
    );

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
