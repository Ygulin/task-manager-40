package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.Session;
import ru.tsc.gulin.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void logout(@Nullable Session session);

}
