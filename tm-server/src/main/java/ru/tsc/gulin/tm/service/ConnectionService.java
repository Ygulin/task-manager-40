package ru.tsc.gulin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.IProjectRepository;
import ru.tsc.gulin.tm.api.repository.ITaskRepository;
import ru.tsc.gulin.tm.api.repository.IUserRepository;
import ru.tsc.gulin.tm.api.repository.ISessionRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IDatabaseProperty;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String username = databaseProperty.getDatabaseUser();
        @Nullable final String password = databaseProperty.getDatabasePassword();
        @Nullable final String url = databaseProperty.getDatabaseUrl();
        @Nullable final String driver = databaseProperty.getDatabaseDriver();
        final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
