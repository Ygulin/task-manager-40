package ru.tsc.gulin.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private User user;

}
