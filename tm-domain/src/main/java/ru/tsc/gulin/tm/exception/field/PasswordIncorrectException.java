package ru.tsc.gulin.tm.exception.field;

public final class PasswordIncorrectException extends AbstractFieldException {

    public PasswordIncorrectException() {
        super("Error! Password is incorrect...");
    }

}
